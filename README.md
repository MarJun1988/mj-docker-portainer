# Portainer Container for Docker

## <u>First Start</u>

Clone the Repository (http)

    git clone https://gitlab.com/MarJun1988/mj-docker-portainer.git

Clone the Repository (ssh)

    git@gitlab.com:MarJun1988/mj-docker-portainer.git

Open the Folder

    cd mj-docker-portainer

Run the docker-compose File with

    docker-compose -p "Portainer" up -d

Explanation of the parameters

    -p "NAME" => Projectname of the Container (Stackname)

    -d => detached mode

## <u>Webinterface for the Portainer Instance</u>

Address in the browser without SSL

[http://127.0.0.1:9000](http://127.0.0.1:9000)

Address in the browser with SSL

[https://127.0.0.1:9443](https://127.0.0.1:9443)
